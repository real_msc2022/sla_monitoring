# SLA MONITORING PROJECT

## SLA stack V1 with docker-compose on aws
### Connect to Grafana stack
- get pem file
- get server ip

```s
ssh -i "~/Downloads/sla_monitoring_aws_1.pem" -L 3009:localhost:3000 ubuntu@ec2-123-123-123-123.compute-1.amazonaws.com
curl curl localhost:3000
```
Test Grafana server with `curl localhost:3000` on remote server.
- please refrain of using X server for the sake of billing

### GRafana + Prometheus stack on docker-compose installation phase
```s
sudo mkdir /app && cd /app
sudo apt update && sudo apt  install docker.io docker-compose -y
git clone https://github.com/grafana/tutorial-environment.git && cd tutorial-environment
docker-compose up --build -d
```

# Sample application for tutorials

This repository contains the environment for completing the tutorials at [grafana.com/tutorials](https://grafana.com/tutorials).

## Prequisites

You will need to have the following installed locally to complete this workshop:

- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

If you're running Docker for Desktop for macOS or Windows, Docker Compose is already included in your installation.

## Running

To start the sample application and the supporting services:

```
docker-compose up -d
```

### Extra tools: 
 - lint Prometheus config file with `./promtool` 

 - Recover testbed files with scp `scp -r -i "~/Downloads/sla-monitoring/sla_monitoring_aws_1.pem" ubuntu@ec2-54-159-115-16.compute-1.amazonaws.com:/app/* .`


### TODO
- [x] Grafana app
- [x] Grafana dashboard
- [x] Node exporter data
- [x] Basic orchestration with Docker-compose
- [ ] Alerting solution
- [ ] Automation
- [ ] Db backup 
- [ ] Connection with AZure VMs